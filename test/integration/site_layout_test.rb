require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "testing the links in the layout" do 
  
    get root_path
    assert_template 'static_pages/home' 
    assert_select "a[href=?]", root_path, count:2
   
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    
    #there should a link to about path and contact, help and 2 links to the home path in the rendered layout i.e HOME layout
    
    
    #verifying that the right template is rendered for the requested path i.e we requested for root path using the get and using the seert_template we are checking if the path we got is correct using the assert_template
    #just remeber that static_pages/home is just checking if there is something called home template and it's not the path, we cant use it in the real app url
    
    
    
  end
  
  
  
end
