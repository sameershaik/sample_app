require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @basename="Rails App"
  end
  
 
  
  test "should get home" do
    get home_path
    assert_response :success #After getting the path the response should be success (200 status code) 
    assert_select "title", "Rails App" #This line means there should be a title tag which contains "Rails App" text in it
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title","About | Rails App"
  end
  
  test "should get help" do
    get help_path
    assert_response :success
  end
  
  test "should get contact" do 
    get contact_path
    assert_response :success
  end
  
  test "check contact page title" do 
    get contact_path #here the route is /contact in the routes.rb file so the helper method is contact_path
    assert_response :success
   # assert_select "title", "contact | Rails App"
  end
  
  test "get the dummy page which uses old routes style" do 
    
    get static_pages_dummy_path #when in doubt regarding which helper to use verify the routes file if the path is /path then helper is path_path or else if the path is /controller/path then the helper will be controller_path_path
    assert_response :success
    
    
  end


end
