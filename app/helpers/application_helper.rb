module ApplicationHelper

    def grabtitle(page_name='')
       
       full_title="Rails App"
        if page_name==''
           full_title
        else
            page_name+" | "+full_title
        end
    end
    
    
end
